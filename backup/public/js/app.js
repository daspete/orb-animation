/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";


var pathData = [[{ x: 115, y: 390 }, { x: 114, y: 405 }, { x: 120, y: 419 }, { x: 131, y: 430 }, { x: 144, y: 439 }, { x: 158, y: 446 }, { x: 173, y: 452 }, { x: 189, y: 458 }, { x: 205, y: 464 }, { x: 221, y: 468 }, { x: 237, y: 472 }, { x: 253, y: 477 }, { x: 269, y: 481 }, { x: 285, y: 485 }, { x: 301, y: 489 }, { x: 317, y: 492 }, { x: 333, y: 496 }, { x: 350, y: 500 }, { x: 367, y: 505 }, { x: 384, y: 509 }, { x: 401, y: 514 }, { x: 418, y: 519 }, { x: 435, y: 524 }, { x: 452, y: 529 }, { x: 469, y: 534 }, { x: 486, y: 540 }, { x: 502, y: 546 }, { x: 519, y: 553 }, { x: 535, y: 559 }, { x: 551, y: 567 }, { x: 567, y: 574 }, { x: 583, y: 582 }, { x: 599, y: 591 }, { x: 614, y: 600 }, { x: 629, y: 610 }], [{ x: 893, y: 351 }, { x: 881, y: 339 }, { x: 867, y: 329 }, { x: 850, y: 321 }, { x: 832, y: 315 }, { x: 815, y: 311 }, { x: 798, y: 309 }, { x: 781, y: 308 }, { x: 762, y: 308 }, { x: 745, y: 309 }, { x: 728, y: 310 }, { x: 711, y: 312 }, { x: 694, y: 314 }, { x: 677, y: 317 }, { x: 660, y: 321 }, { x: 644, y: 325 }, { x: 627, y: 329 }, { x: 609, y: 334 }, { x: 592, y: 339 }, { x: 576, y: 344 }, { x: 559, y: 350 }, { x: 541, y: 356 }, { x: 524, y: 363 }, { x: 506, y: 370 }, { x: 490, y: 377 }, { x: 474, y: 385 }, { x: 458, y: 392 }, { x: 441, y: 402 }, { x: 425, y: 410 }, { x: 410, y: 419 }, { x: 395, y: 428 }, { x: 378, y: 438 }, { x: 362, y: 449 }, { x: 347, y: 459 }, { x: 331, y: 471 }, { x: 315, y: 483 }, { x: 300, y: 496 }, { x: 285, y: 509 }, { x: 270, y: 522 }, { x: 255, y: 536 }, { x: 241, y: 550 }, { x: 227, y: 564 }, { x: 214, y: 579 }, { x: 203, y: 593 }, { x: 193, y: 608 }, { x: 185, y: 622 }, { x: 177, y: 637 }, { x: 171, y: 651 }, { x: 165, y: 667 }, { x: 162, y: 682 }, { x: 161, y: 698 }, { x: 162, y: 713 }], [{ x: 763, y: 414 }, { x: 746, y: 417 }, { x: 729, y: 419 }, { x: 711, y: 422 }, { x: 692, y: 426 }, { x: 675, y: 430 }, { x: 657, y: 434 }, { x: 638, y: 440 }, { x: 620, y: 445 }, { x: 602, y: 452 }, { x: 586, y: 458 }, { x: 570, y: 464 }, { x: 554, y: 471 }, { x: 537, y: 479 }, { x: 521, y: 486 }, { x: 506, y: 493 }, { x: 489, y: 502 }, { x: 472, y: 511 }, { x: 456, y: 520 }, { x: 439, y: 530 }, { x: 424, y: 539 }, { x: 408, y: 548 }, { x: 393, y: 558 }, { x: 379, y: 568 }, { x: 363, y: 579 }, { x: 348, y: 592 }, { x: 334, y: 603 }, { x: 321, y: 615 }, { x: 308, y: 627 }, { x: 296, y: 640 }, { x: 284, y: 653 }, { x: 273, y: 667 }, { x: 262, y: 681 }, { x: 252, y: 695 }, { x: 243, y: 710 }, { x: 235, y: 726 }, { x: 228, y: 743 }, { x: 223, y: 760 }, { x: 222, y: 777 }, { x: 224, y: 795 }, { x: 231, y: 811 }, { x: 242, y: 824 }, { x: 256, y: 834 }, { x: 271, y: 842 }, { x: 288, y: 848 }, { x: 305, y: 852 }, { x: 321, y: 854 }, { x: 337, y: 855 }, { x: 355, y: 855 }, { x: 372, y: 854 }, { x: 388, y: 853 }, { x: 404, y: 851 }, { x: 421, y: 848 }, { x: 437, y: 845 }, { x: 453, y: 842 }, { x: 469, y: 838 }], [{ x: 108, y: 565 }, { x: 113, y: 581 }, { x: 119, y: 598 }, { x: 126, y: 615 }, { x: 132, y: 630 }, { x: 140, y: 645 }, { x: 149, y: 661 }, { x: 159, y: 676 }, { x: 169, y: 690 }, { x: 181, y: 703 }, { x: 193, y: 716 }, { x: 206, y: 728 }, { x: 220, y: 739 }, { x: 234, y: 749 }, { x: 249, y: 759 }, { x: 265, y: 768 }, { x: 281, y: 775 }, { x: 297, y: 782 }, { x: 316, y: 789 }, { x: 335, y: 796 }, { x: 355, y: 801 }, { x: 375, y: 807 }, { x: 392, y: 810 }, { x: 409, y: 814 }, { x: 429, y: 818 }, { x: 449, y: 821 }, { x: 469, y: 823 }, { x: 487, y: 825 }, { x: 504, y: 827 }, { x: 524, y: 828 }, { x: 545, y: 829 }, { x: 565, y: 830 }, { x: 586, y: 830 }, { x: 606, y: 830 }, { x: 626, y: 829 }, { x: 644, y: 828 }, { x: 664, y: 826 }, { x: 682, y: 824 }, { x: 699, y: 822 }, { x: 719, y: 819 }, { x: 739, y: 814 }, { x: 759, y: 809 }, { x: 778, y: 803 }, { x: 797, y: 796 }], [{ x: 861, y: 275 }, { x: 869, y: 290 }, { x: 874, y: 307 }, { x: 874, y: 326 }, { x: 871, y: 344 }, { x: 866, y: 361 }, { x: 858, y: 378 }, { x: 849, y: 394 }, { x: 839, y: 409 }, { x: 828, y: 424 }, { x: 817, y: 438 }, { x: 805, y: 452 }, { x: 792, y: 465 }, { x: 779, y: 478 }, { x: 765, y: 490 }, { x: 750, y: 504 }, { x: 735, y: 515 }, { x: 719, y: 528 }, { x: 704, y: 539 }, { x: 689, y: 550 }, { x: 674, y: 560 }, { x: 659, y: 570 }, { x: 643, y: 579 }, { x: 628, y: 589 }, { x: 612, y: 598 }, { x: 596, y: 607 }, { x: 577, y: 617 }, { x: 561, y: 625 }, { x: 544, y: 633 }, { x: 528, y: 640 }, { x: 511, y: 648 }, { x: 494, y: 655 }, { x: 477, y: 662 }, { x: 460, y: 668 }, { x: 443, y: 674 }, { x: 425, y: 680 }, { x: 408, y: 685 }, { x: 390, y: 690 }, { x: 372, y: 695 }, { x: 355, y: 699 }, { x: 337, y: 702 }, { x: 319, y: 705 }, { x: 301, y: 708 }, { x: 282, y: 710 }, { x: 264, y: 711 }, { x: 243, y: 711 }, { x: 225, y: 710 }, { x: 207, y: 708 }, { x: 189, y: 704 }, { x: 171, y: 699 }, { x: 155, y: 691 }, { x: 140, y: 680 }, { x: 128, y: 667 }, { x: 120, y: 650 }, { x: 117, y: 632 }, { x: 118, y: 614 }, { x: 122, y: 596 }, { x: 128, y: 579 }, { x: 136, y: 563 }, { x: 145, y: 547 }, { x: 155, y: 532 }, { x: 166, y: 517 }, { x: 178, y: 503 }, { x: 192, y: 488 }, { x: 205, y: 475 }, { x: 218, y: 462 }, { x: 232, y: 450 }, { x: 246, y: 438 }, { x: 260, y: 426 }, { x: 275, y: 415 }, { x: 289, y: 404 }, { x: 304, y: 394 }, { x: 319, y: 384 }, { x: 335, y: 374 }, { x: 350, y: 364 }, { x: 366, y: 355 }, { x: 382, y: 346 }, { x: 398, y: 337 }, { x: 414, y: 328 }, { x: 431, y: 320 }, { x: 447, y: 312 }, { x: 463, y: 305 }, { x: 479, y: 298 }, { x: 496, y: 291 }, { x: 512, y: 284 }, { x: 529, y: 278 }, { x: 546, y: 272 }, { x: 563, y: 266 }, { x: 580, y: 261 }, { x: 597, y: 256 }, { x: 614, y: 252 }, { x: 631, y: 247 }, { x: 649, y: 244 }, { x: 666, y: 241 }, { x: 684, y: 238 }, { x: 700, y: 236 }, { x: 716, y: 235 }, { x: 732, y: 234 }], [{ x: 133, y: 390 }, { x: 123, y: 404 }, { x: 115, y: 418 }, { x: 108, y: 432 }, { x: 101, y: 447 }, { x: 97, y: 463 }, { x: 95, y: 479 }, { x: 97, y: 495 }, { x: 102, y: 511 }, { x: 111, y: 524 }, { x: 124, y: 536 }, { x: 140, y: 545 }, { x: 157, y: 551 }, { x: 173, y: 554 }, { x: 189, y: 557 }, { x: 205, y: 558 }, { x: 223, y: 558 }, { x: 240, y: 557 }, { x: 256, y: 556 }, { x: 273, y: 554 }, { x: 289, y: 552 }, { x: 305, y: 549 }, { x: 321, y: 545 }, { x: 339, y: 541 }, { x: 356, y: 536 }, { x: 372, y: 531 }, { x: 388, y: 526 }, { x: 405, y: 520 }, { x: 422, y: 513 }, { x: 439, y: 507 }, { x: 455, y: 500 }, { x: 472, y: 492 }, { x: 488, y: 484 }, { x: 504, y: 477 }, { x: 521, y: 468 }, { x: 536, y: 460 }, { x: 552, y: 451 }, { x: 568, y: 442 }, { x: 583, y: 432 }, { x: 598, y: 423 }, { x: 613, y: 413 }, { x: 628, y: 402 }, { x: 643, y: 391 }, { x: 657, y: 380 }, { x: 670, y: 368 }, { x: 683, y: 356 }, { x: 696, y: 342 }, { x: 707, y: 330 }, { x: 718, y: 318 }, { x: 730, y: 305 }, { x: 740, y: 292 }, { x: 750, y: 278 }, { x: 759, y: 264 }, { x: 766, y: 249 }, { x: 771, y: 233 }, { x: 774, y: 217 }, { x: 775, y: 201 }], [{ x: 649, y: 168 }, { x: 633, y: 170 }, { x: 617, y: 173 }, { x: 601, y: 175 }, { x: 584, y: 178 }, { x: 568, y: 182 }, { x: 551, y: 185 }, { x: 535, y: 190 }, { x: 519, y: 194 }, { x: 503, y: 199 }, { x: 487, y: 205 }, { x: 471, y: 210 }, { x: 455, y: 216 }, { x: 440, y: 223 }, { x: 425, y: 229 }, { x: 410, y: 236 }, { x: 395, y: 243 }, { x: 380, y: 251 }, { x: 365, y: 259 }, { x: 350, y: 267 }, { x: 336, y: 276 }, { x: 322, y: 284 }, { x: 306, y: 293 }, { x: 292, y: 303 }, { x: 277, y: 312 }, { x: 263, y: 321 }, { x: 249, y: 331 }, { x: 235, y: 341 }, { x: 221, y: 352 }, { x: 208, y: 363 }, { x: 195, y: 374 }, { x: 183, y: 385 }, { x: 172, y: 397 }, { x: 161, y: 410 }, { x: 150, y: 423 }, { x: 139, y: 437 }, { x: 130, y: 451 }, { x: 122, y: 465 }, { x: 116, y: 480 }, { x: 110, y: 495 }, { x: 105, y: 510 }, { x: 103, y: 525 }, { x: 103, y: 541 }, { x: 107, y: 556 }, { x: 116, y: 569 }, { x: 128, y: 580 }, { x: 142, y: 588 }, { x: 156, y: 595 }, { x: 171, y: 600 }, { x: 186, y: 604 }, { x: 201, y: 606 }, { x: 217, y: 608 }, { x: 234, y: 608 }, { x: 252, y: 608 }, { x: 268, y: 606 }, { x: 284, y: 605 }, { x: 300, y: 602 }, { x: 316, y: 599 }, { x: 332, y: 596 }, { x: 348, y: 592 }, { x: 365, y: 587 }, { x: 381, y: 582 }, { x: 397, y: 577 }, { x: 414, y: 571 }], [{ x: 372, y: 155 }, { x: 363, y: 170 }, { x: 354, y: 186 }, { x: 344, y: 204 }, { x: 335, y: 222 }, { x: 325, y: 240 }, { x: 318, y: 256 }, { x: 310, y: 272 }, { x: 303, y: 288 }, { x: 295, y: 304 }, { x: 289, y: 321 }, { x: 282, y: 337 }, { x: 276, y: 354 }, { x: 270, y: 371 }, { x: 264, y: 390 }, { x: 258, y: 407 }, { x: 254, y: 424 }, { x: 249, y: 441 }, { x: 245, y: 459 }, { x: 242, y: 476 }, { x: 238, y: 494 }, { x: 236, y: 511 }, { x: 233, y: 529 }, { x: 232, y: 547 }, { x: 231, y: 564 }, { x: 230, y: 584 }, { x: 230, y: 602 }, { x: 231, y: 620 }, { x: 234, y: 638 }, { x: 236, y: 656 }, { x: 240, y: 673 }, { x: 245, y: 691 }, { x: 251, y: 708 }, { x: 257, y: 725 }, { x: 265, y: 741 }, { x: 273, y: 757 }, { x: 282, y: 773 }, { x: 292, y: 787 }, { x: 303, y: 802 }, { x: 315, y: 816 }, { x: 327, y: 829 }, { x: 340, y: 841 }, { x: 354, y: 852 }], [{ x: 491, y: 751 }, { x: 473, y: 752 }, { x: 456, y: 753 }, { x: 440, y: 755 }, { x: 423, y: 757 }, { x: 407, y: 759 }, { x: 390, y: 761 }, { x: 374, y: 764 }, { x: 357, y: 766 }, { x: 341, y: 769 }, { x: 324, y: 771 }, { x: 308, y: 774 }, { x: 291, y: 776 }, { x: 275, y: 779 }, { x: 258, y: 781 }, { x: 242, y: 784 }, { x: 227, y: 785 }, { x: 212, y: 784 }, { x: 198, y: 776 }, { x: 203, y: 760 }, { x: 213, y: 747 }, { x: 223, y: 735 }, { x: 235, y: 722 }, { x: 247, y: 710 }, { x: 259, y: 698 }, { x: 271, y: 685 }, { x: 283, y: 673 }, { x: 295, y: 661 }, { x: 307, y: 648 }, { x: 319, y: 636 }, { x: 331, y: 623 }, { x: 342, y: 610 }, { x: 353, y: 597 }, { x: 365, y: 584 }, { x: 376, y: 571 }, { x: 387, y: 557 }, { x: 398, y: 544 }, { x: 408, y: 530 }, { x: 419, y: 516 }, { x: 430, y: 502 }, { x: 440, y: 488 }, { x: 449, y: 474 }, { x: 459, y: 459 }, { x: 468, y: 444 }, { x: 477, y: 427 }, { x: 485, y: 412 }, { x: 493, y: 396 }, { x: 500, y: 381 }, { x: 506, y: 364 }, { x: 512, y: 348 }, { x: 518, y: 330 }, { x: 523, y: 313 }, { x: 527, y: 296 }, { x: 529, y: 279 }, { x: 531, y: 262 }, { x: 531, y: 244 }], [{ x: 288, y: 845 }, { x: 278, y: 833 }, { x: 272, y: 818 }, { x: 270, y: 803 }, { x: 272, y: 788 }, { x: 277, y: 773 }, { x: 284, y: 758 }, { x: 290, y: 743 }, { x: 297, y: 727 }, { x: 304, y: 712 }, { x: 311, y: 697 }, { x: 318, y: 681 }, { x: 325, y: 666 }, { x: 331, y: 650 }, { x: 338, y: 635 }, { x: 345, y: 619 }, { x: 352, y: 604 }, { x: 359, y: 588 }, { x: 365, y: 573 }, { x: 372, y: 557 }, { x: 379, y: 540 }, { x: 386, y: 524 }, { x: 392, y: 508 }, { x: 399, y: 491 }, { x: 405, y: 476 }, { x: 411, y: 459 }, { x: 418, y: 442 }, { x: 424, y: 425 }, { x: 429, y: 407 }, { x: 434, y: 391 }, { x: 438, y: 375 }, { x: 442, y: 359 }, { x: 445, y: 343 }, { x: 448, y: 325 }, { x: 450, y: 308 }, { x: 452, y: 290 }, { x: 451, y: 272 }, { x: 450, y: 255 }, { x: 447, y: 239 }, { x: 443, y: 222 }, { x: 436, y: 206 }, { x: 426, y: 192 }, { x: 415, y: 180 }, { x: 401, y: 172 }, { x: 385, y: 167 }, { x: 369, y: 165 }, { x: 353, y: 166 }, { x: 337, y: 168 }, { x: 321, y: 172 }, { x: 305, y: 178 }, { x: 290, y: 185 }, { x: 275, y: 193 }, { x: 260, y: 202 }, { x: 245, y: 213 }, { x: 231, y: 223 }, { x: 217, y: 234 }, { x: 204, y: 245 }, { x: 191, y: 257 }, { x: 179, y: 270 }, { x: 168, y: 283 }, { x: 158, y: 297 }, { x: 150, y: 311 }, { x: 141, y: 326 }, { x: 134, y: 342 }, { x: 128, y: 357 }, { x: 123, y: 373 }, { x: 118, y: 389 }, { x: 115, y: 406 }, { x: 113, y: 423 }, { x: 112, y: 440 }, { x: 112, y: 457 }, { x: 113, y: 474 }, { x: 116, y: 490 }, { x: 119, y: 507 }, { x: 123, y: 525 }, { x: 129, y: 543 }, { x: 135, y: 559 }, { x: 141, y: 574 }, { x: 149, y: 590 }, { x: 156, y: 605 }, { x: 165, y: 619 }, { x: 174, y: 634 }, { x: 184, y: 648 }, { x: 195, y: 662 }, { x: 206, y: 676 }, { x: 218, y: 688 }, { x: 231, y: 700 }, { x: 244, y: 711 }, { x: 258, y: 722 }, { x: 272, y: 732 }, { x: 286, y: 742 }, { x: 300, y: 751 }, { x: 314, y: 759 }, { x: 329, y: 768 }, { x: 343, y: 776 }, { x: 359, y: 784 }, { x: 375, y: 792 }, { x: 390, y: 799 }, { x: 405, y: 805 }]];

/*

let pathData = [
  [
    { x: -75.5, y: 184.5 },
    { x: -89.5, y: 216.5 },
    { x: -93.5, y: 235.5 },
    { x: -54, y: 258 },
    { x: -15.849999999999994, y: 279.73 },
    { x: 59, y: 296 },
    { x: 136, y: 315 },
    { x: 213, y: 334 },
    { x: 344.5, y: 367.5 },
    { x: 437.5, y: 434.5 }
  ],
  [
    { x: 704.5, y: 190.5 },
    { x: 687.5, y: 133.5 },
    { x: 587, y: 94 },
    { x: 376, y: 162 },
    { x: 165, y: 230 },
    { x: 31, y: 371 },
    { x: 2, y: 413 },
    { x: -27, y: 455 },
    { x: -51.5, y: 500.5 },
    { x: -32.5, y: 548.5 }
  ],
  [
    { x: 588.5, y: 230.5 },
    { x: 455.5, y: 236.5 },
    { x: 342, y: 286 },
    { x: 215, y: 362 },
    { x: 88, y: 438 },
    { x: -21, y: 578 },
    { x: 38, y: 638 },
    { x: 97, y: 698 },
    { x: 230, y: 666 },
    { x: 278, y: 654 }
  ],
  [
    { x: -96, y: 367 },
    { x: -81, y: 421 },
    { x: -52, y: 531 },
    { x: 78, y: 592 },
    { x: 208, y: 653 },
    { x: 463, y: 671 },
    { x: 600, y: 613 }
  ],
  [
    { x: 642.5, y: 75.5 },
    { x: 652.5, y: 80.5 },
    { x: 663.14, y: 94.86 },
    { x: 667.23, y: 102.98 },
    { x: 708.38, y: 184.54000000000002 },
    { x: 575.4, y: 334.58 },
    { x: 370.23, y: 438.1 },
    { x: 165.06000000000006, y: 541.6200000000001 },
    { x: -34.64999999999998, y: 559.4300000000001 },
    { x: -75.76999999999998, y: 477.87 },
    { x: -116.88999999999999, y: 396.30999999999995 },
    { x: 16.060000000000016, y: 246.27 },
    { x: 221.23000000000002, y: 142.75 },
    { x: 335, y: 85.31 },
    { x: 461.5, y: 47.5 },
    { x: 548.5, y: 52.5 }
  ],
  [
    { x: -58, y: 195 },
    { x: -92, y: 240 },
    { x: -125, y: 297 },
    { x: -89, y: 342 },
    { x: -53, y: 387 },
    { x: 60, y: 385 },
    { x: 173, y: 349 },
    { x: 286, y: 313 },
    { x: 425, y: 236 },
    { x: 489, y: 168 },
    { x: 553, y: 100 },
    { x: 585.5, y: 54.5 },
    { x: 573.5, y: -1.5 }
  ],
  [
    { x: 464, y: -15 },
    { x: 393, y: -8.5 },
    { x: 329, y: 5.75 },
    { x: 264.5, y: 30.62 },
    { x: 200, y: 55.49 },
    { x: 135, y: 91 },
    { x: 62, y: 140 },
    { x: -11, y: 189 },
    { x: -54.5, y: 239.75 },
    { x: -77.12, y: 282.5 },
    { x: -99.74000000000001, y: 325.25 },
    { x: -101.5, y: 360 },
    { x: -91, y: 377 },
    { x: -80.5, y: 394 },
    { x: -55.75, y: 415.25 },
    { x: -7.75, y: 423.12 },
    { x: 40.25, y: 430.99 },
    { x: 121, y: 423 },
    { x: 224.5, y: 386.5 }
  ],
  [
    { x: 168, y: 679 },
    { x: 152, y: 672 },
    { x: 26, y: 581 },
    { x: 30, y: 395 },
    { x: 36, y: 200 },
    { x: 147.5, y: 14.5 },
    { x: 185.5, y: -48.5 }
  ],
  [
    { x: 306, y: 570 },
    { x: 285, y: 562 },
    { x: 110, y: 592 },
    { x: 54, y: 600 },
    { x: -2, y: 608 },
    { x: -21.549999999999997, y: 602.35 },
    { x: 22, y: 554 },
    { x: 65.55000000000001, y: 505.65 },
    { x: 122.86000000000001, y: 458.81 },
    { x: 214, y: 341 },
    { x: 305.29, y: 223 },
    { x: 340.5, y: 109.5 },
    { x: 328.5, y: 41.5 }
  ],
  [
    { x: 98.5, y: 673.5 },
    { x: 83.5, y: 659.5 },
    { x: 57.5, y: 637.5 },
    { x: 77.5, y: 590.5 },
    { x: 97.5, y: 543.5 },
    { x: 168.5, y: 386.5 },
    { x: 189.5, y: 332.5 },
    { x: 210.5, y: 278.5 },
    { x: 275.5, y: 133.5 },
    { x: 242.5, y: 39.5 },
    { x: 209.5, y: -54.5 },
    { x: 103.5, y: -9.5 },
    { x: 61.5, y: 19.5 },
    { x: 19.5, y: 48.5 },
    { x: -51.5, y: 93.5 },
    { x: -80.5, y: 202.5 },
    { x: -109.5, y: 311.5 },
    { x: -51.5, y: 427.5 },
    { x: 2.5, y: 489.5 },
    { x: 56.5, y: 551.5 },
    { x: 154.5, y: 603.5 },
    { x: 215.5, y: 627.5 }
  ]
];*/
/* harmony default export */ __webpack_exports__["a"] = (pathData);

/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Vector__ = __webpack_require__(2);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }



var Trail = function () {
    function Trail() {
        var config = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

        _classCallCheck(this, Trail);

        this.defaults = {
            id: null,
            path: null,
            animationTime: 3,
            colors: ['rgba(255,255,255,1)', 'rgba(130,135,255,0.2)', 'rgba(130,135,255,0.1)', 'rgba(130,135,255,0)'],
            radius: 8
        };

        this.config = Object.assign(this.defaults, config);

        this.pointId = 0;
        this.tick = 0;
        this.endTick = 0;
        this.pointTickTime = 16;

        this.lastPoint = this.config.path[0];
    }

    _createClass(Trail, [{
        key: 'update',
        value: function update(ctx) {
            if (this.pointId >= this.config.path.length - 1) {
                this.tick++;
                if (this.endTick == 0) {
                    this.endTick = this.tick;
                    console.log(this.config.id, this.endTick);
                }
                var circleActive = this.drawCircle(ctx);

                if (circleActive == false) {
                    return false;
                } else {
                    return true;
                }
            }
            this.lastPoint = {
                x: this.config.path[this.pointId].x,
                y: this.config.path[this.pointId].y
            };

            this.tick++;

            this.pointId++; //= 10;

            if (this.pointId >= this.config.path.length - 1) {
                this.pointId = this.config.path.length - 1;
            }

            this.draw(ctx);

            return true;
        }
    }, {
        key: 'draw',
        value: function draw(ctx) {
            var radius = this.config.radius;
            var lastPoint = this.lastPoint;
            var currentPoint = this.config.path[this.pointId];

            var distance = __WEBPACK_IMPORTED_MODULE_0__Vector__["a" /* default */].distance(lastPoint, currentPoint);
            var angle = __WEBPACK_IMPORTED_MODULE_0__Vector__["a" /* default */].angle(lastPoint, currentPoint);

            for (var i = 0; i < distance; i += radius * 0.1) {
                var x = lastPoint.x + Math.sin(angle) * i;
                var y = lastPoint.y + Math.cos(angle) * i;

                y += 8;
                x -= 2;

                if (this.config.colors.length > 1) {
                    var gradient = ctx.createRadialGradient(x, y, radius * 0.5, x, y, radius);

                    for (var j = 0; j < this.config.colors.length; j++) {
                        var index = j / (this.config.colors.length - 1);

                        gradient.addColorStop(index, this.config.colors[j]);
                    }

                    ctx.fillStyle = gradient;
                } else {
                    ctx.fillStyle = this.config.colors[0];
                }

                ctx.fillRect(x - radius, y - radius, radius * 2, radius * 2);
            }
        }
    }, {
        key: 'drawCircle',
        value: function drawCircle(ctx) {
            var currentTick = this.tick - this.endTick;

            if (currentTick >= this.pointTickTime) {
                return false;
            }

            var ratio = (currentTick + 1) / this.pointTickTime;
            var radius = 20 * ratio;

            var pathEndPoint = this.config.path[this.config.path.length - 1];

            ctx.beginPath();

            if (this.config.colors.length > 1) {
                var gradient = ctx.createRadialGradient(pathEndPoint.x, pathEndPoint.y + 8, radius * 0.1, pathEndPoint.x, pathEndPoint.y + 8, radius);

                for (var j = 0; j < this.config.colors.length; j++) {
                    var index = j / (this.config.colors.length - 1);

                    gradient.addColorStop(index, this.config.colors[j]);
                }

                ctx.fillStyle = gradient;
            } else {
                ctx.fillStyle = this.config.colors[0];
            }

            ctx.arc(pathEndPoint.x, pathEndPoint.y + 8, radius, false, Math.PI * 2, false);
            ctx.fill();

            ctx.closePath();

            return true;
        }
    }]);

    return Trail;
}();

/* harmony default export */ __webpack_exports__["a"] = (Trail);

/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Vector = function () {
    function Vector() {
        _classCallCheck(this, Vector);
    }

    _createClass(Vector, null, [{
        key: "distance",
        value: function distance(point1, point2) {
            return Math.sqrt(Math.pow(point2.x - point1.x, 2) + Math.pow(point2.y - point1.y, 2));
        }
    }, {
        key: "angle",
        value: function angle(point1, point2) {
            return Math.atan2(point2.x - point1.x, point2.y - point1.y);
        }
    }]);

    return Vector;
}();

/* harmony default export */ __webpack_exports__["a"] = (Vector);

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(4);
module.exports = __webpack_require__(7);


/***/ }),
/* 4 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__vendor_curveCalc__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__classes_Trail__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__classes_Renderer__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__classes_Vector__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__data_pathReduced__ = __webpack_require__(0);








var pathes = [];

var pathConfigs = [{
  // 0
  points: 64,
  pointDivider: 48,
  radius: 4,
  colors: ["rgba(255,255,255,1)", "rgba(130,135,255,0.2)", "rgba(130,135,255,0.1)", "rgba(130,135,255,0)"]
}, {
  // 1
  points: 64,
  pointDivider: 72,
  radius: 4,
  colors: ["rgba(255,255,255,1)", "rgba(130,135,255,0.2)", "rgba(130,135,255,0.1)", "rgba(130,135,255,0)"]
}, {
  // 2
  points: 64,
  pointDivider: 76,
  radius: 4,
  colors: ["rgba(255,255,255,1)", "rgba(130,135,255,0.2)", "rgba(130,135,255,0.1)", "rgba(130,135,255,0)"]
}, {
  // 3
  points: 64,
  pointDivider: 60,
  radius: 4,
  colors: ["rgba(255,255,255,1)", "rgba(130,135,255,0.2)", "rgba(130,135,255,0.1)", "rgba(130,135,255,0)"]
}, {
  // 4
  points: 64,
  pointDivider: 132,
  radius: 4,
  colors: ["rgba(255,255,255,1)", "rgba(130,135,255,0.2)", "rgba(130,135,255,0.1)", "rgba(130,135,255,0)"]
}, {
  // 5
  points: 64,
  pointDivider: 78,
  radius: 4,
  colors: ["rgba(255,255,255,1)", "rgba(130,135,255,0.2)", "rgba(130,135,255,0.1)", "rgba(130,135,255,0)"]
}, {
  // 6
  points: 64,
  pointDivider: 86,
  radius: 4,
  colors: ["rgba(255,255,255,1)", "rgba(130,135,255,0.2)", "rgba(130,135,255,0.1)", "rgba(130,135,255,0)"]
}, {
  // 7
  points: 64,
  pointDivider: 58,
  radius: 4,
  colors: ["rgba(255,255,255,1)", "rgba(130,135,255,0.2)", "rgba(130,135,255,0.1)", "rgba(130,135,255,0)"]
}, {
  // 8
  points: 64,
  pointDivider: 78,
  radius: 4,
  colors: ["rgba(255,255,255,1)", "rgba(130,135,255,0.2)", "rgba(130,135,255,0.1)", "rgba(130,135,255,0)"]
}, {
  // 9
  points: 64,
  pointDivider: 130,
  radius: 4,
  colors: ["rgba(255,255,255,1)", "rgba(130,135,255,0.2)", "rgba(130,135,255,0.1)", "rgba(130,135,255,0)"]
}];

__WEBPACK_IMPORTED_MODULE_4__data_pathReduced__["a" /* default */].forEach(function (path, pathId) {
  var p = [];

  path.forEach(function (point) {
    p.push(point.x /* + 200*/);
    p.push(point.y /* + 180*/);
  });

  var curvedPath = Object(__WEBPACK_IMPORTED_MODULE_0__vendor_curveCalc__["a" /* default */])(p, 0.5, pathConfigs[pathId].points);
  var compiledPath = [];

  for (var x = 0; x < curvedPath.length; x += pathConfigs[pathId].pointDivider) {
    compiledPath.push({ x: curvedPath[x], y: curvedPath[x + 1] });
  }

  pathes.push(compiledPath);
});

//console.log(pathes);

// let pathes = [];

// pathData.forEach((path) => {
//     let currentPath = [];

//     for(let x = 0; x < path.length; x++){

//         for(let xx = x; xx < path.length; xx++){
//             if(Vector.distance(path[x], path[xx]) > 15){
//                 x = xx;
//                 currentPath.push(path[xx]);
//                 break;
//             }
//         }

//     }

//     pathes.push(currentPath);
// });

// console.log(JSON.stringify(pathes));


var animate = function animate() {
  document.getElementById('canvas').classList.remove('fadeout');
  document.getElementById('orb').classList.remove('fadein');

  renderer.clean();

  for (var i = 0; i < pathes.length; i++) {
    var radius = 5;
    var ticks = 1;

    switch (i) {
      case 0:
        radius = 4;ticks = 1;break;
      case 1:
        radius = 3;ticks = 1;break;
      case 2:
        radius = 2;ticks = 1;break;
      case 3:
        radius = 5;ticks = 1;break;
      case 4:
        radius = 7;ticks = 1;break;
      case 5:
        radius = 3;ticks = 1;break;
      case 6:
        radius = 4;ticks = 1;break;
      case 7:
        radius = 5;ticks = 1;break;
      case 8:
        radius = 4;ticks = 1;break;
      case 9:
        radius = 3;ticks = 1;break;

    }

    renderer.addGameObject(new __WEBPACK_IMPORTED_MODULE_1__classes_Trail__["a" /* default */]({
      path: pathes[i],
      radius: radius,
      id: i,
      colors: pathConfigs[i].colors
    }));
  }

  renderer.start();
};

var renderer = new __WEBPACK_IMPORTED_MODULE_2__classes_Renderer__["a" /* default */]({
  onReady: function onReady() {
    document.getElementById('orb').classList.add('fadein');
  },
  onComplete: function onComplete() {
    document.getElementById('canvas').classList.add('fadeout');

    // setTimeout(() => { 
    //     animate();
    // }, 1000);
  }
});

animate();

/***/ }),
/* 5 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/*!	Curve calc function for canvas 2.3.7
 *	(c) Epistemex 2013-2016
 *	www.epistemex.com
 *	License: MIT
 */

/**
 * Calculates an array containing points representing a cardinal spline through given point array.
 * Points must be arranged as: [x1, y1, x2, y2, ..., xn, yn].
 *
 * There must be a minimum of two points in the input array but the function
 * is only useful where there are three points or more.
 *
 * The points for the cardinal spline are returned as a new array.
 *
 * @param {Array} points - point array
 * @param {Number} [tension=0.5] - tension. Typically between [0.0, 1.0] but can be exceeded
 * @param {Number} [numOfSeg=25] - number of segments between two points (line resolution)
 * @param {Boolean} [close=false] - Close the ends making the line continuous
 * @returns {Float32Array} New array with the calculated points that was added to the path
 */
function getCurvePoints(points, tension, numOfSeg, close) {
  "use strict";

  if (typeof points === "undefined" || points.length < 2) return new Float32Array(0);

  // options or defaults
  tension = typeof tension === "number" ? tension : 0.5;
  numOfSeg = typeof numOfSeg === "number" ? numOfSeg : 25;

  var pts,
      // for cloning point array
  i = 1,
      l = points.length,
      rPos = 0,
      rLen = (l - 2) * numOfSeg + 2 + (close ? 2 * numOfSeg : 0),
      res = new Float32Array(rLen),
      cache = new Float32Array(numOfSeg + 2 << 2),
      cachePtr = 4;

  pts = points.slice(0);

  if (close) {
    pts.unshift(points[l - 1]); // insert end point as first point
    pts.unshift(points[l - 2]);
    pts.push(points[0], points[1]); // first point as last point
  } else {
    pts.unshift(points[1]); // copy 1. point and insert at beginning
    pts.unshift(points[0]);
    pts.push(points[l - 2], points[l - 1]); // duplicate end-points
  }

  // cache inner-loop calculations as they are based on t alone
  cache[0] = 1; // 1,0,0,0

  for (; i < numOfSeg; i++) {
    var st = i / numOfSeg,
        st2 = st * st,
        st3 = st2 * st,
        st23 = st3 * 2,
        st32 = st2 * 3;

    cache[cachePtr++] = st23 - st32 + 1; // c1
    cache[cachePtr++] = st32 - st23; // c2
    cache[cachePtr++] = st3 - 2 * st2 + st; // c3
    cache[cachePtr++] = st3 - st2; // c4
  }

  cache[++cachePtr] = 1; // 0,1,0,0

  // calc. points
  parse(pts, cache, l, tension);

  if (close) {
    //l = points.length;
    pts = [];
    pts.push(points[l - 4], points[l - 3], points[l - 2], points[l - 1], // second last and last
    points[0], points[1], points[2], points[3]); // first and second
    parse(pts, cache, 4, tension);
  }

  function parse(pts, cache, l, tension) {
    for (var i = 2, t; i < l; i += 2) {
      var pt1 = pts[i],
          pt2 = pts[i + 1],
          pt3 = pts[i + 2],
          pt4 = pts[i + 3],
          t1x = (pt3 - pts[i - 2]) * tension,
          t1y = (pt4 - pts[i - 1]) * tension,
          t2x = (pts[i + 4] - pt1) * tension,
          t2y = (pts[i + 5] - pt2) * tension,
          c = 0,
          c1,
          c2,
          c3,
          c4;

      for (t = 0; t < numOfSeg; t++) {
        c1 = cache[c++];
        c2 = cache[c++];
        c3 = cache[c++];
        c4 = cache[c++];

        res[rPos++] = c1 * pt1 + c2 * pt3 + c3 * t1x + c4 * t2x;
        res[rPos++] = c1 * pt2 + c2 * pt4 + c3 * t1y + c4 * t2y;
      }
    }
  }

  // add last point
  l = close ? 0 : points.length - 2;
  res[rPos++] = points[l++];
  res[rPos] = points[l];

  return res;
}

//if (typeof exports !== "undefined") exports.getCurvePoints = getCurvePoints;
/* harmony default export */ __webpack_exports__["a"] = (getCurvePoints);

/***/ }),
/* 6 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Trail__ = __webpack_require__(1);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }



var Renderer = function () {
    function Renderer() {
        var config = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

        _classCallCheck(this, Renderer);

        this.defaults = {
            canvasId: 'canvas',
            width: 1000,
            height: 1000,
            onComplete: function onComplete() {}
        };

        this.config = Object.assign(this.defaults, config);

        this.canvas = document.getElementById(this.config.canvasId);

        if (!canvas) {
            console.warn('CANVAS not found');
            return;
        }

        this.canvas.width = this.config.width;
        this.canvas.height = this.config.height;

        this.ctx = this.canvas.getContext('2d');
        this.ctx.lineJoin = this.ctx.lineCap = 'round';

        this.gameObjects = [];

        this.active = false;
    }

    _createClass(Renderer, [{
        key: 'addGameObject',
        value: function addGameObject(gameObject) {
            this.gameObjects.push(gameObject);
        }
    }, {
        key: 'start',
        value: function start() {
            this.active = true;

            this.ctx.globalCompositeOperation = 'destination-out';
            this.ctx.fillStyle = 'rgba(255,255,255,1)';
            this.ctx.fillRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);

            this.update();
        }
    }, {
        key: 'clean',
        value: function clean() {
            this.gameObjects = [];
        }
    }, {
        key: 'update',
        value: function update() {
            var _this = this;

            if (this.active == false) return;

            this.ctx.globalCompositeOperation = 'destination-out';
            this.ctx.fillStyle = 'rgba(255,255,255,0.12)';
            this.ctx.fillRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);

            this.ctx.globalCompositeOperation = 'soft-light';

            var isOneGameObjectActive = false;

            this.gameObjects.forEach(function (gameObject) {
                var isGameObjectActive = gameObject.update(_this.ctx);

                if (isGameObjectActive == true) {
                    isOneGameObjectActive = true;
                } else {
                    _this.config.onReady();
                }
            });

            if (isOneGameObjectActive == false) {
                this.active = false;
                this.config.onComplete();
                return;
            }

            requestAnimationFrame(function () {
                _this.update();
            });
        }
    }]);

    return Renderer;
}();

/* harmony default export */ __webpack_exports__["a"] = (Renderer);

/***/ }),
/* 7 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ })
/******/ ]);