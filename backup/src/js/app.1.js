import Trail from './classes/Trail'
import Renderer from './classes/Renderer'

import pathData from './data/pathReduced'


let animate = () => {
    document.getElementById('canvas').classList.remove('fadeout');
    document.getElementById('orb').classList.remove('fadein');

    renderer.clean();

    for(let i = 0; i  < pathData.length; i++){
        let radius = 5;
    
        switch(i){
            case 0: radius = 4; break;
            case 1: radius = 3; break;
            case 2: radius = 2; break;
            case 3: radius = 5; break;
            case 4: radius = 7; break;
            case 5: radius = 3; break;
            case 6: radius = 4; break;
            case 7: radius = 5; break;
            case 8: radius = 4; break;
            case 9: radius = 3; break;
            
        }
    
        renderer.addGameObject(new Trail({
            path: pathData[i],
            radius: radius
        }));
    }
    
    renderer.start();
}

let renderer = new Renderer({
    onReady(){
        document.getElementById('orb').classList.add('fadein');
    },

    onComplete(){
        document.getElementById('canvas').classList.add('fadeout');

        // setTimeout(() => { 
        //     animate();
        // }, 1000);
    }
});

animate();