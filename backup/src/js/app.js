import getCurvePoints from "./vendor/curveCalc";

import Trail from './classes/Trail'
import Renderer from './classes/Renderer'
import Vector from './classes/Vector'

import pathData from './data/pathReduced'

let pathes = [];

let pathConfigs = [
  {
    // 0
    points: 64,
    pointDivider: 48,
    radius: 4,
    colors: [
      "rgba(255,255,255,1)",
      "rgba(130,135,255,0.2)",
      "rgba(130,135,255,0.1)",
      "rgba(130,135,255,0)"
    ]
  },
  {
    // 1
    points: 64,
    pointDivider: 72,
    radius: 4,
    colors: [
      "rgba(255,255,255,1)",
      "rgba(130,135,255,0.2)",
      "rgba(130,135,255,0.1)",
      "rgba(130,135,255,0)"
    ]
  },
  {
    // 2
    points: 64,
    pointDivider: 76,
    radius: 4,
    colors: [
      "rgba(255,255,255,1)",
      "rgba(130,135,255,0.2)",
      "rgba(130,135,255,0.1)",
      "rgba(130,135,255,0)"
    ]
  },
  {
    // 3
    points: 64,
    pointDivider: 60,
    radius: 4,
    colors: [
      "rgba(255,255,255,1)",
      "rgba(130,135,255,0.2)",
      "rgba(130,135,255,0.1)",
      "rgba(130,135,255,0)"
    ]
  },
  {
    // 4
    points: 64,
    pointDivider: 132,
    radius: 4,
    colors: [
      "rgba(255,255,255,1)",
      "rgba(130,135,255,0.2)",
      "rgba(130,135,255,0.1)",
      "rgba(130,135,255,0)"
    ]
  },
  {
    // 5
    points: 64,
    pointDivider: 78,
    radius: 4,
    colors: [
      "rgba(255,255,255,1)",
      "rgba(130,135,255,0.2)",
      "rgba(130,135,255,0.1)",
      "rgba(130,135,255,0)"
    ]
  },
  {
    // 6
    points: 64,
    pointDivider: 86,
    radius: 4,
    colors: [
      "rgba(255,255,255,1)",
      "rgba(130,135,255,0.2)",
      "rgba(130,135,255,0.1)",
      "rgba(130,135,255,0)"
    ]
  },
  {
    // 7
    points: 64,
    pointDivider: 58,
    radius: 4,
    colors: [
      "rgba(255,255,255,1)",
      "rgba(130,135,255,0.2)",
      "rgba(130,135,255,0.1)",
      "rgba(130,135,255,0)"
    ]
  },
  {
    // 8
    points: 64,
    pointDivider: 78,
    radius: 4,
    colors: [
      "rgba(255,255,255,1)",
      "rgba(130,135,255,0.2)",
      "rgba(130,135,255,0.1)",
      "rgba(130,135,255,0)"
    ]
  },
  {
    // 9
    points: 64,
    pointDivider: 130,
    radius: 4,
    colors: [
      "rgba(255,255,255,1)",
      "rgba(130,135,255,0.2)",
      "rgba(130,135,255,0.1)",
      "rgba(130,135,255,0)"
    ]
  }
];

pathData.forEach((path, pathId) => {
    let p = [];

    path.forEach((point) => {
        p.push(point.x/* + 200*/);
        p.push(point.y/* + 180*/);
    });

    let curvedPath = getCurvePoints(p, 0.5, pathConfigs[pathId].points);
    let compiledPath = [];

    for (let x = 0; x < curvedPath.length; x += pathConfigs[pathId].pointDivider) {
      compiledPath.push({ x: curvedPath[x], y: curvedPath[x + 1] });
    }

    pathes.push(compiledPath);
    
});


//console.log(pathes);

// let pathes = [];

// pathData.forEach((path) => {
//     let currentPath = [];

//     for(let x = 0; x < path.length; x++){
        
//         for(let xx = x; xx < path.length; xx++){
//             if(Vector.distance(path[x], path[xx]) > 15){
//                 x = xx;
//                 currentPath.push(path[xx]);
//                 break;
//             }
//         }

//     }

//     pathes.push(currentPath);
// });

// console.log(JSON.stringify(pathes));





let animate = () => {
    document.getElementById('canvas').classList.remove('fadeout');
    document.getElementById('orb').classList.remove('fadein');

    renderer.clean();

    for(let i = 0; i  < pathes.length; i++){
        let radius = 5;
        let ticks = 1;
    
        switch(i){
            case 0: radius = 4; ticks = 1; break;
            case 1: radius = 3; ticks = 1; break;
            case 2: radius = 2; ticks = 1; break;
            case 3: radius = 5; ticks = 1; break;
            case 4: radius = 7; ticks = 1; break;
            case 5: radius = 3; ticks = 1; break;
            case 6: radius = 4; ticks = 1; break;
            case 7: radius = 5; ticks = 1; break;
            case 8: radius = 4; ticks = 1; break;
            case 9: radius = 3; ticks = 1; break;
            
        }
    
        renderer.addGameObject(new Trail({
            path: pathes[i],
            radius: radius,
            id: i,
            colors: pathConfigs[i].colors
        }));
    }
    
    renderer.start();
}

let renderer = new Renderer({
    onReady(){
        document.getElementById('orb').classList.add('fadein');
    },

    onComplete(){
        document.getElementById('canvas').classList.add('fadeout');

        // setTimeout(() => { 
        //     animate();
        // }, 1000);
    }
});

animate();