import Trail from './Trail'

class Renderer {

    constructor(config = {}){
        this.defaults = {
            canvasId: 'canvas',
            width: 1000,
            height: 1000,
            onComplete: () => {}
        };

        this.config = Object.assign(this.defaults, config);

        this.canvas = document.getElementById(this.config.canvasId);

        if(!canvas){
            console.warn('CANVAS not found');
            return;
        }

        this.canvas.width = this.config.width;
        this.canvas.height = this.config.height;

        this.ctx = this.canvas.getContext('2d');
        this.ctx.lineJoin = this.ctx.lineCap = 'round';

        this.gameObjects = [];

        this.active = false;
    }

    addGameObject(gameObject){
        this.gameObjects.push(gameObject);
    }

    start(){
        this.active = true;

        this.ctx.globalCompositeOperation = 'destination-out';
        this.ctx.fillStyle = 'rgba(255,255,255,1)';
        this.ctx.fillRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);

        this.update();
    }

    clean(){
        this.gameObjects = [];
    }

    update(){
        if(this.active == false) return;

        this.ctx.globalCompositeOperation = 'destination-out';
        this.ctx.fillStyle = 'rgba(255,255,255,0.12)';
        this.ctx.fillRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);

        this.ctx.globalCompositeOperation = 'soft-light';

        let isOneGameObjectActive = false;

        this.gameObjects.forEach((gameObject) => {
            let isGameObjectActive = gameObject.update(this.ctx);

            if(isGameObjectActive == true){
                isOneGameObjectActive = true;
            }else{
                this.config.onReady();
            }
        });

        if(isOneGameObjectActive == false){
            this.active = false;
            this.config.onComplete();
            return;
        }

        requestAnimationFrame(() => { this.update() });
    }

    

}

export default Renderer;