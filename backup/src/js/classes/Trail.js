import Vector from './Vector'

class Trail {

    constructor(config = {}){
        this.defaults = {
            id: null,
            path: null,
            animationTime: 3,
            colors: [ 
                'rgba(255,255,255,1)',
                'rgba(130,135,255,0.2)',
                'rgba(130,135,255,0.1)',
                'rgba(130,135,255,0)'
            ],
            radius: 8
        };

        this.config = Object.assign(this.defaults, config);

        this.pointId = 0;
        this.tick = 0;
        this.endTick = 0;
        this.pointTickTime = 16;

        this.lastPoint = this.config.path[0];
    }

    update(ctx){
        if(this.pointId >= this.config.path.length - 1){
            this.tick++;
            if(this.endTick == 0){
                this.endTick = this.tick;
                console.log(this.config.id, this.endTick);
            }
            let circleActive = this.drawCircle(ctx);

            if(circleActive == false){
                return false;
            }else{
                return true;
            }
            
        }
        this.lastPoint = { 
            x: this.config.path[this.pointId].x, 
            y: this.config.path[this.pointId].y 
        };

        this.tick++;

        this.pointId++;//= 10;

        if(this.pointId >= this.config.path.length - 1){
            this.pointId = this.config.path.length - 1;
        }
        

        this.draw(ctx);

        return true;
    }

    draw(ctx){
        let radius = this.config.radius;
        let lastPoint = this.lastPoint;
        let currentPoint = this.config.path[this.pointId];

        let distance = Vector.distance(lastPoint, currentPoint);
        let angle = Vector.angle(lastPoint, currentPoint);
        
        for(let i = 0; i < distance; i += radius * 0.1){
            let x = lastPoint.x + Math.sin(angle) * i;
            let y = lastPoint.y + Math.cos(angle) * i;

            y += 8;
            x -= 2;

            if(this.config.colors.length > 1){
                let gradient = ctx.createRadialGradient(x, y, radius * 0.5, x, y, radius);

                for(let j = 0; j < this.config.colors.length; j++){
                    let index = j / (this.config.colors.length - 1);

                    gradient.addColorStop(index, this.config.colors[j]);
                }

                ctx.fillStyle = gradient;
            }else{
                ctx.fillStyle = this.config.colors[0];
            }

            ctx.fillRect(x - radius, y - radius, radius * 2, radius * 2);
        }
    }

    drawCircle(ctx){
        let currentTick = this.tick - this.endTick;

        if(currentTick >= this.pointTickTime){
            return false;
        }

        let ratio = (currentTick + 1) / this.pointTickTime;
        let radius = 20 * ratio;

        let pathEndPoint = this.config.path[this.config.path.length - 1];

        ctx.beginPath();

        if(this.config.colors.length > 1){
            let gradient = ctx.createRadialGradient(pathEndPoint.x, pathEndPoint.y + 8, radius * 0.1, pathEndPoint.x, pathEndPoint.y + 8, radius);

            for(let j = 0; j < this.config.colors.length; j++){
                let index = j / (this.config.colors.length - 1);

                gradient.addColorStop(index, this.config.colors[j]);
            }

            ctx.fillStyle = gradient;
        }else{
            ctx.fillStyle = this.config.colors[0];
        }

        ctx.arc(pathEndPoint.x, pathEndPoint.y + 8, radius, false, Math.PI * 2, false);
        ctx.fill();

        ctx.closePath();



        return true;
    }

}

export default Trail;