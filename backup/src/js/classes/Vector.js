class Vector {
    static distance(point1, point2){
        return Math.sqrt(Math.pow(point2.x - point1.x, 2) + Math.pow(point2.y - point1.y, 2));
    }

    static angle(point1, point2){
        return Math.atan2(point2.x - point1.x, point2.y - point1.y);
    }
}

export default Vector;