import {
    Engine,
    Scene,
    Vector3,
    ArcRotateCamera,
    Color3,
    Color4,
    Curve3,
    Path3D,
    Animation,
    ParticleSystem,
    Texture,
    Mesh,
    MeshBuilder
} from 'babylonjs'

import pathData from './data/pathReduced'

let canvas = document.getElementById('canvas');
let engine = new Engine(canvas, true);

let createScene = () => {
    let scene = new Scene(engine);

    let camera = new ArcRotateCamera('camera', 0, 0, 10, Vector3.Zero(), scene);
    camera.setPosition(new Vector3(0, 0, -1200));
    camera.lowerBetaLimit = 0.1;
    camera.upperBetaLimit = (Math.PI / 2) * 0.99;
    camera.lowerRadiusLimit = 5;

    scene.clearColor = new Color4(0,0,0,0);

    let emitters = [];

    pathData.forEach((path, pathId) => {
        let emitter = Mesh.CreateBox('emitter' + pathId, 0.1, scene);
        emitter.isVisible = false;

        let particleSystem = new ParticleSystem("particles", 10000, scene);
        particleSystem.particleTexture = new Texture("assets/fire.jpg", scene);
        particleSystem.minAngularSpeed = -2;
        particleSystem.maxAngularSpeed = 2;
        particleSystem.minSize = 10;
        particleSystem.maxSize = 50;
        particleSystem.minLifeTime = 0.01;
        particleSystem.maxLifeTime = 0.55;
        particleSystem.minEmitPower = 6;
        particleSystem.maxEmitPower = 16;
        particleSystem.emitter = emitter;
        particleSystem.emitRate = 1200;
        particleSystem.blendMode = ParticleSystem.BLENDMODE_ONEONE;
        particleSystem.minEmitBox = new Vector3(0, 0, 0);
        particleSystem.maxEmitBox = new Vector3(0, 0, 0);
        particleSystem.direction1 = new Vector3(-5, -5, -5);
        particleSystem.direction2 = new Vector3(5, 5, 5);
        particleSystem.color1 = new Color3(0, 0.4, 1);
        particleSystem.color2 = new Color4(0.05, 0.7, 0.95, 0);
        particleSystem.gravity = new Vector3(0, 0, 0);
        particleSystem.start();

        scene.stopAnimation(emitter);

        let currentPath = [];
        for(let x = 0; x < path.length; x++){
            currentPath.push(new Vector3(path[x].x - 500, -path[x].y + 492, 0));
        }

        let catmull = Curve3.CreateCatmullRomSpline(currentPath, 20);
        //let spline = Mesh.CreateLines('catmull', catmull.getPoints(), scene);

        let path3d = new Path3D(catmull.getPoints());

        const speed = 1000;
        const animationPosition = new Animation('positionAnim', 'position', speed, Animation.ANIMATIONTYPE_VECTOR3, Animation.ANIMATIONLOOPMODE_CYCLE);
        const positionKeys = [];

        for(let x = 0; x < catmull.getPoints().length; x++){
            positionKeys.push({
                frame: x,
                value: catmull.getPoints()[x]
            });
        }

        animationPosition.setKeys(positionKeys);

        emitter.animations = [ animationPosition ];

        scene.beginAnimation(emitter, 0, catmull.getPoints().length, false);
    });

    // var particleSystem = new ParticleSystem("particles", 10000, scene);
    //     particleSystem.particleTexture = new Texture("assets/fire.jpg", scene);
    //     particleSystem.minAngularSpeed = -2;
    //     particleSystem.maxAngularSpeed = 2;
    //     particleSystem.minSize = 19;
    //     particleSystem.maxSize = 29;
    //     particleSystem.minLifeTime = 0.01;
    //     particleSystem.maxLifeTime = 0.55;
    //     particleSystem.minEmitPower = 6;
    //     particleSystem.maxEmitPower = 16;
    //     particleSystem.emitter = emitter;
    //     particleSystem.emitRate = 1000;
    //     particleSystem.blendMode = ParticleSystem.BLENDMODE_ONEONE;
    //     particleSystem.minEmitBox = new Vector3(0, 0, 0);
    //     particleSystem.maxEmitBox = new Vector3(0, 0, 0);
    //     particleSystem.direction1 = new Vector3(-5, -5, -5);
    //     particleSystem.direction2 = new Vector3(5, 5, 5);
    //     particleSystem.color1 = new Color3(0, 0.4, 1);
    //     particleSystem.color2 = new Color3(0.05, 0.7, 0.95);
    //     particleSystem.gravity = new Vector3(0, 0, 0);
    //     particleSystem.start();

    //emitter.position = Vector3.Zero();

    //let sphere = BABYLON.MeshBuilder.CreateSphere("sphere", {diameter:10, updatable: true}, scene);

    // scene.stopAnimation(emitter);

    // let path = [];

    // for(let x = 0; x < pathData[0].length; x++){
    //     path.push(new Vector3(pathData[0][x].x, pathData[0][x].y, 0));
    // }

    // let catmull = Curve3.CreateCatmullRomSpline(path, path.length);
    // let spline = Mesh.CreateLines('catmull', catmull.getPoints(), scene);

    // let path3d = new Path3D(catmull.getPoints());

    // const speed = 2000;
    // const animationPosition = new Animation('positionAnim', 'position', speed, Animation.ANIMATIONTYPE_VECTOR3, Animation.ANIMATIONLOOPMODE_CYCLE);
    // const positionKeys = [];

    // for(let x = 0; x < catmull.getPoints().length; x++){
    //     positionKeys.push({
    //         frame: x,
    //         value: catmull.getPoints()[x]
    //     });
    // }

    // animationPosition.setKeys(positionKeys);

    // emitter.animations = [ animationPosition ];

    // scene.beginAnimation(emitter, 0, catmull.getPoints().length, false);

    return scene;
}

let scene = createScene();

engine.runRenderLoop(function(){
    scene.render();
})
