import Renderer from './app/Renderer'
import PathConverter from './app/PathConverter'
import Trail from './app/Trail'
import Shape from './app/Shape'
import pathes from './data/path'
const {
    Color,
    Vector3
} = THREE;

let renderer = new Renderer();




pathes.forEach((path, pathId) => {
    renderer.addGameObject(new Trail({
        id: pathId,
        radius: 180,
        colors: [
            // [29/255,52/255,102/255,1],
            // [29/255,52/255,102/255,0],
            [1,1,1,1],
            [1,1,1,0]
        ],
        textureSize: [0.01, 1],
        time: 2,
        length: 40,
        path: new PathConverter(path),
        shape: Shape.createCircle(10, 16)
    }));
});



renderer.start();