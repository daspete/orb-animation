class PathConverter {

    constructor(path){
        let _path = [];

        path.forEach((point) => {
            _path.push({
                x: point.x,
                y: 1000-point.y
            })
        });
        return _path;
    }

}

export default PathConverter