const {

    Scene,
    PerspectiveCamera,
    Vector3,
    Matrix4,
    TextureLoader,
    Mesh,
    Shape,
    WebGLRenderer

} = THREE;

class Renderer {

    constructor(config = {}) {
        this.defaults = {
            canvasId: 'animation-container',
            width: 1000,
            height: 1000,
            onComplete: () => { }
        };

        this.config = Object.assign(this.defaults, config);

        this.renderer = new WebGLRenderer({
            antialias: true,
            alpha: true
        });
        this.renderer.setSize(this.config.width, this.config.height);
        this.rendererContainer = document.getElementById(this.config.canvasId);
        this.rendererContainer.appendChild(this.renderer.domElement);

        this.gameObjects = [];

        this.scene = null;
        this.camera = null;
    }

    addGameObject(gameObject) {
        this.gameObjects.push(gameObject);
    }

    start() {
        this.scene = new Scene();
        this.camera = new PerspectiveCamera(45, 1, 2, 2000);
        this.scene.add(this.camera);
        this.camera.position.set(508, 500, 1210);

        this.gameObjects.forEach((gameObject) => {
            gameObject.start(this.renderer, this.scene);
        });

        this.animate();

    }

    animate(){
        requestAnimationFrame(() => { this.animate() });
        this.update();
        this.render();
    }


    update(){
        this.gameObjects.forEach((gameObject) => {
            gameObject.update();
        });
    }

    render(){
        this.renderer.render(this.scene, this.camera);
    }




}

export default Renderer;