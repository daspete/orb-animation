const {
    Color,
    Vector3
} = THREE;

class Shape {

    static createCircle(radius, segments){
        let circlePoints = [];
        let scale = radius;
        let inc = (Math.PI * 2) / segments;
        for(let i = 0; i < Math.PI * 2 + inc; i += inc){
            let position = new Vector3();
            position.set(Math.cos(i) * scale, Math.sin(i) * scale, 0);
            circlePoints.push(position);
        }

        return circlePoints;
    }

}

export default Shape;