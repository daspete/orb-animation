const {
    TrailRenderer,
    TextureLoader,
    RepeatWrapping,
    Mesh,
    Vector3,
    Object3D,
    Matrix4
} = THREE;

class Trail {

    constructor(data){
        this.id = data.id;
        this.path = data.path;
        this.radius = data.radius;
        this.colors = data.colors;
        this.time = data.time;
        this.length = data.length;
        this.shape = data.shape;
        this.textureSize = data.textureSize;

        this.position = { x: this.path[0].x, y: this.path[0].y };

        this.renderer = null;
        this.scene = null;
        this.trailRenderer = null;
        this.material = null;
        this.textureLoader = null;
        this.texture = null;
        
        this.target = null;
        this.tick = 0;


        this.forward = new Vector3(0, 0, -1);

        this._forward = new Vector3();
        this._up = new Vector3();

        this._translationMatrix = new Matrix4();

        this.lastTargetPosition = new Vector3();

        this.currentDirection = new Vector3();
        this.lastDirection = new Vector3();

        this.active = true;
    }

    start(renderer, scene){
        this.renderer = renderer;
        this.scene = scene;
        this.trailRenderer = new TrailRenderer(this.scene, false);
        this.material = TrailRenderer.createBaseMaterial();

        this.afterStart();


        // this.textureLoader = new TextureLoader();

        // this.textureLoader.load('assets/line.png', (tex) => {
        //     tex.wrapS = RepeatWrapping;
        //     tex.wrapT = RepeatWrapping;

        //     this.material = TrailRenderer.createTexturedMaterial();
        //     this.material.uniforms.texture.value = tex;

        //     this.texture = tex;

        //     this.afterStart();
        // });
    }

    afterStart(){
        this.target = new Object3D();
        this.target.position.set(this.path[0].x, this.path[0].y, 0);

        this.scene.add(this.target);

        this.trailRenderer.initialize(
            this.material,
            Math.floor(this.length),
            0,
            0,
            this.shape,
            this.target,
        );

        this.updateColors();

        this.trailRenderer.activate();

        this.startTween();
    }

    startTween(){
        
        this.target.position.set(this.path[0].x, this.path[0].y, 0);
        this.position = { x: this.path[0].x, y: this.path[0].y };
        
        this.active = true;

        TweenMax.to(this.position, this.time, {
            bezier: {
                values: this.path
            },
            ease: Power3.easeIn,

            onComplete: () => {
                this.active = false;

                //setTimeout(() => { this.startTween() }, 1000);
            }
        });
    }

    updateColors(){
        this.material.uniforms.headColor.value.set(
            this.colors[0][0],
            this.colors[0][1],
            this.colors[0][2],
            this.colors[0][3]
        );

        this.material.uniforms.tailColor.value.set(
            this.colors[1][0],
            this.colors[1][1],
            this.colors[1][2],
            this.colors[1][3]
        );

        this.material.uniforms.textureTileFactor.value.set(
            this.textureSize[0],
            this.textureSize[1]
        );

        this.material.depthWrite = false;
    }

    update(){
        if(this.target == null) return;
        //if(this.active == false) return;

        this.trailRenderer.advance();
        this.trailRenderer.updateHead();

        this._translationMatrix.identity();

        this.lastTargetPosition.copy(this.position);

        this.lastDirection.copy(this.currentDirection);
        this.currentDirection.copy(this.position);
        this.currentDirection.sub(this.lastTargetPosition);
        this.currentDirection.normalize();

        this._up.crossVectors(this.currentDirection, this.forward);

        let angle = this.forward.angleTo(this.currentDirection);

        this._translationMatrix.makeTranslation(this.position.x, this.position.y, 0);

        this.target.matrix.identity();
        this.target.applyMatrix(this._translationMatrix);
        this.target.updateMatrixWorld();

        this.tick++;
    }

}

export default Trail